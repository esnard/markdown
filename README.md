# From Markdown to Beautiful HTML

Dans le contexte de l'enseignement, voici une démo minimaliste de comment écrire
des exercices en Markdown. L'objectif est de publier automatiquement sur des
*Gitlab Pages* deux versions des exercices au format HTML, l'une avec la
correction et l'autre sans. Pour ce faire, nous allons réaliser la conversion en
HTML avec *pandoc*.

## Bibliographie

* <https://github.com/KrauseFx/markdown-to-html-github-style>
* <https://www.arthurperret.fr/enseignement-automatisation-pandoc.html>
* Pandoc : <https://pandoc.org/MANUAL.html>
* Extensions Pandoc au Markdown : <https://pandoc.org/MANUAL.html#pandocs-markdown>

## Un petit Markdown pour commencer

Voici le source de la démo en Markdown : [demo.md](demo.md).


Voici la ligne de commande *pandoc* à utiliser pour convertir le fichier
`demo.md` en HTML de base.

```
pandoc -s -t html4 --metadata pagetitle="demo" demo.md -o demo.html
```

Le résultat est alors le suivant : <https://esnard.gitlabpages.inria.fr/markdown/demo.html>.

## Affichage Conditionnel de la Solution

La syntaxe Markdown (avec le *flavor* *pandoc*) permet d'utiliser des classes
CSS, comme `solution`, directement dans le source du fichier Markdown. C'est
très pratique ! Considérons ce petit exemple.

```md
# Exercice

Voici un exercice en apparence très difficile.

::: solution
Voici la solution, en fait très simple.
:::
```

Si on convertit ce petit document Markdown en HTML, on voit un bloc `<div>` avec
 la classe CSS `solution` :
 <https://esnard.gitlabpages.inria.fr/markdown/demo.html>

```html
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title>demo</title>
</head>
<body>
<h1 id="exercice">Exercice</h1>
<p>Voici un exercice en apparence très difficile.</p>
<div class="solution">
<p>Voici la solution, en fait très simple.</p>
</div>
</body>
</html>
```

On peut ensuite ajouter un filtre LUA qui va cacher (ou pas) les éléments de la
classe CSS `solution`. : [solution.lua](solution.lua).

```
pandoc -s -t html4 --metadata pagetitle="demo" demo.md -o demo-solution.html
pandoc -s -t html4 --metadata pagetitle="demo" demo.md --lua-filter=solution.lua -o demo-nosolution.html
```

Le résultat est alors le suivant : <https://esnard.gitlabpages.inria.fr/markdown/demo-nosolution.html>.

On peut ensuite appliquer une feuille de style CSS pour la classe `solution` :
[solution.css](solution.css).

```
pandoc -s -t html4 --metadata pagetitle="demo" demo.md --css solution.css -o demo-solution.html
```

Le résultat est alors le suivant :  <https://esnard.gitlabpages.inria.fr/markdown/demo-solution.html>.

Un peu d'aide sur l'écriture des filtres en LUA : <https://pandoc.org/lua-filters.html>

Il est aussi possible d'écrire un filtre Pandoc en Python avec le module
[panflute](http://scorreia.com/software/panflute/guide.html).

## Application d'un style CSS

Nous allons maintenant considérer un style CSS proche de celui utilisé pour
afficher le Markdown dans GitHub : [style.css](style.css).

Voici la ligne de commande *pandoc* à utiliser pour convertir le fichier
`demo.md` en HTML en appliquant ce style.

```
pandoc -s -t html4 --metadata pagetitle="demo" demo.md --css style.css -o demo-style.html
```

Le résultat est alors le suivant : <https://esnard.gitlabpages.inria.fr/markdown/demo-style.html>.

## Un peu de raffinement

Mettons tout ensemble et ajoutons encore quelques options *pandoc*...

```bash
pandoc -s -t html4 --metadata pagetitle="demo" demo.md                  \
    --mathml --highlight-style=tango --css style.css --css solution.css \
    --toc --toc-depth 2 -o demo-final.html
```

Un peu de documentation : <https://pandoc.org/MANUAL.html#syntax-highlighting>

Voici le résultat final (avec solution) :
<https://esnard.gitlabpages.inria.fr/markdown/demo-final.html>. Il ne reste plus
qu'à trouver son style CSS personnel... Enjoy :-)

## A regarder encore...

* https://github.com/Mushiyo/pandoc-toc-sidebar
* ...

