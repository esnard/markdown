# Exercices

## Exercice 1

Quel est le résultat de la commande `ping -n www.perdu.com` ?

::: solution
Voici le résultat :

```text
PING www.perdu.com (208.97.177.124) 56(84) bytes of data.
64 bytes from 208.97.177.124: icmp_seq=1 ttl=50 time=98.3 ms
64 bytes from 208.97.177.124: icmp_seq=2 ttl=50 time=98.4 ms
```

:::

## Exercice 2

Considérons le code suivant en langage C. Que fait-il ?

```C
#include <stdio.h>
#define N 10

int main(int argc, char const *argv[])
{
    for (int i = 0; i < N; i++)
        printf("hello world\n");
    return 0;
}
```

::: solution
Il affiche 10 fois `hello world` à la ligne.
:::

## Exercice 3

Considérons le labyrinthe ci-dessous. Trouvez le bon chemin.

![](laby.png)

::: solution
C'est le chemin 2.
:::

## Exercice 4

Considérons la formule suivante :

$$
\binom{n}{k} = \frac{n!}{k!(n-k)!}
$$

Quel est la valeur de $\binom{3}{2}$ ?

::: solution
La réponse est 3.
:::

## Exercice 5

Un exemple de tableau HTML...

<table>
<tr><th>col1</th><th>col2</th><th>col3</th></tr>
<tr><td>val1</td><td>val2</td><td>val3</td></tr>
<tr><td>val4</td><td>val5</td><td>val6</td></tr>
</table>
